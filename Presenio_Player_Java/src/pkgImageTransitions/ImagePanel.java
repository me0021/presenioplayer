package pkgImageTransitions;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JPanel;
import pkgImageTransitions.Transitions.Trans_CrossDissolve;
import pkgImageTransitions.Transitions.Trans_WipeDown;
import pkgImageTransitions.Transitions.Trans_WipeLeft;
import pkgImageTransitions.Transitions.Trans_WipeRight;
import pkgImageTransitions.Transitions.Trans_WipeUp;

//============================================================================
/** This class implements the panel on which all images are drawn */
//===========================================================================
public class ImagePanel extends JPanel
{
	/** the parent JFrame */
	private JFrame m_Parent;

	/** The image to draw*/
	private BufferedImage theImage = null;

	/** Screen panel width */
	protected int m_iPanelWidth;

	/** Screen panel height */

	protected int m_iPanelHeight;

	/** Current image being displayed on the screen */
	public BufferedImage m_CurrentImage;
	
	/** Next image to be displayed after this transition */
	public BufferedImage m_NextImage;

    /** Next image to be displayed after this transition */
    public BufferedImage finalImage;

    //----- Transition variables -----
	/** Vector of transition objects */
	private Vector<Transition> m_vTransitions;
    /* Vector of transition lengths */
	private Vector<Long> m_vTransitionLength;
	/* The tranistion index set from current index in Presenio Player */
	public int transIdx;
	// The current transition time
	private long transTime;
	//--------------------------------------------------------
	/** Default constructor */
	//--------------------------------------------------------
	public ImagePanel(JFrame parent)
	{
	    // initialize the transition index to zero
	    this.transIdx = 0;
		m_Parent = parent;
		this.setSize(m_Parent.getSize().width-5, 
				m_Parent.getSize().height-60);   // Set the size 
		this.setLocation(0, 0);       // Set the location in the window
		this.setBackground(Color.BLACK); // Set the panel color
		this.setLayout(null); // No layout manager
		m_iPanelWidth = this.getWidth();    // get panel width
		m_iPanelHeight = this.getHeight();  // get panel height
		// Create the off-screen BufferedImages for use by the transitions
		m_CurrentImage = new BufferedImage(m_iPanelWidth, m_iPanelHeight, BufferedImage.TYPE_INT_RGB);
		m_NextImage = new BufferedImage(m_iPanelWidth, m_iPanelHeight, BufferedImage.TYPE_INT_RGB);
		m_CurrentImage.getGraphics().setColor(Color.BLACK);
		m_CurrentImage.getGraphics().drawRect(0, 0, m_iPanelWidth, m_iPanelHeight);
	}

    //--------------------------------------------------------
    /** Parse the array of image transitions and
     * build a vector object containing transition objects
     * Try and build as few objects as possible*/
    //--------------------------------------------------------
    public void setImageTransitions(Vector<String> imageTransitions) {
	    m_vTransitions = new Vector<Transition>(); // build a blank vector
        Transition baseTrans = new Transition(this); // Make sure everything gets initialized here
        m_vTransitions.add(baseTrans); // the first transition will always be instant
        // Create base transition objects
        Trans_WipeLeft twl = new Trans_WipeLeft();
        Trans_WipeRight twr = new Trans_WipeRight();
        Trans_WipeUp twu = new Trans_WipeUp();
        Trans_WipeDown twd = new Trans_WipeDown();
        Trans_CrossDissolve tcd = new Trans_CrossDissolve();
        // Cycle through the array and assign transitions to each place.
        // This is based on image transitions read from the save file.
        for(int i = 0; i < imageTransitions.size(); i++) {
            switch(imageTransitions.get(i)) {
                case "wipe_left": {
                    m_vTransitions.add(twl);
                    break;
                }
                case "wipe_right": {
                    m_vTransitions.add(twr);
                    break;
                }
                case "wipe_up": {
                    m_vTransitions.add(twu);
                    break;
                }
                case "wipe_down": {
                    m_vTransitions.add(twd);
                    break;
                }
                case "cross_dissolve": {
                    m_vTransitions.add(tcd);
                    break;
                }
                case "no_transition": {
                    m_vTransitions.add(baseTrans);
                    break;
                }
            }
        }
    }

    //--------------------------------------------------------
    /** Receive the transition index from Presenio Player
     * Update the transition index here */
    //--------------------------------------------------------
    public void setCurIdx(int idx) {
        transIdx = idx;
    }

    //--------------------------------------------------------
    /** Receive an array of transition lengths for the current
     * slideshow */
    //--------------------------------------------------------
    public void setTransLengthVector(Vector<Long> lengths) { m_vTransitionLength = lengths;}

    //--------------------------------------------------------
	/** Set the reference to the image to be drawn. */
	//--------------------------------------------------------
	public void setImage(BufferedImage nextImg)
	{
		theImage = nextImg;
        if(theImage == null) { // If it's the last 2 elements
            finalImage = new BufferedImage(m_iPanelWidth, m_iPanelHeight, BufferedImage.TYPE_INT_RGB);
            //create a blank next image
            finalImage.getGraphics().setColor(Color.BLACK);
            Graphics gNext = finalImage.getGraphics();
            gNext.drawImage(nextImg, 0, 0, m_iPanelWidth, m_iPanelHeight, this);
            m_NextImage = finalImage; // set black frame to next image
            transTime = 2;          // manually set the transition time
        } else if (transIdx >= m_vTransitionLength.size()) { // if you're going back from the last black screen
            // create another black frame
            BufferedImage blackFrame = new BufferedImage(m_iPanelWidth, m_iPanelHeight, BufferedImage.TYPE_INT_RGB);
            blackFrame.getGraphics().setColor(Color.BLACK);
            Graphics gNext2 = blackFrame.getGraphics();
            gNext2.drawImage(blackFrame, 0, 0, m_iPanelWidth, m_iPanelHeight, this);
            m_CurrentImage = blackFrame; // set as the first image in the pair of 2
            int imgWidth = theImage.getWidth(this);    // this panel is the ImageObserver required.
            int imgHeight = theImage.getHeight(this);
            int posX, posY; // Position to place upper-left corner of image
            int imgScaleWidth, imgScaleHeight; // In case we need to scale - Using class vars temporarily
            // Set the upper left position of the image
            if (imgWidth < m_iPanelWidth)
                posX = (m_iPanelWidth - imgWidth) / 2;
            else
                posX = 0;
            if (imgHeight < m_iPanelHeight)
                posY = (m_iPanelHeight - imgHeight) / 2;
            else
                posY = 0;
            // See if we need to scale it
            if ((posX == 0) || (posY == 0)) {
                if (imgWidth > imgHeight) // Scale by width
                {
                    imgScaleWidth = m_iPanelWidth;
                    imgScaleHeight = (int) Math.round(((double) m_iPanelWidth / (double) imgWidth) *
                            (double) imgHeight);
                } else // Scale by height
                {
                    imgScaleHeight = m_iPanelHeight;
                    imgScaleWidth = (int) Math.round(((double) m_iPanelHeight / (double) imgHeight) *
                            (double) imgWidth);
                }
            } else // Draw it normal size
            {
                imgScaleWidth = imgWidth;
                imgScaleHeight = imgHeight;
            }
            // set transition time for next to last element
            transTime = m_vTransitionLength.elementAt(this.transIdx-1);
            // prepare the images for the functions
            Graphics gNext = m_NextImage.getGraphics();
            gNext.drawImage(nextImg, posX, posY, imgScaleWidth, imgScaleHeight, this);
        } else { // this is an image within the slideshow. handle normally.
            // Scale and copy this into the m_NextImage buffer
            // Note: If the image is too large or too small we try to scale it
            int imgWidth = theImage.getWidth(this);    // this panel is the ImageObserver required.
            int imgHeight = theImage.getHeight(this);
            int posX, posY; // Position to place upper-left corner of image
            int imgScaleWidth, imgScaleHeight; // In case we need to scale - Using class vars temporarily
            // Professor's original code below: //
            // Set the upper left position of the image
            if (imgWidth < m_iPanelWidth)
                posX = (m_iPanelWidth - imgWidth) / 2;
            else
                posX = 0;
            if (imgHeight < m_iPanelHeight)
                posY = (m_iPanelHeight - imgHeight) / 2;
            else
                posY = 0;
            // See if we need to scale it
            if ((posX == 0) || (posY == 0)) {
                if (imgWidth > imgHeight) // Scale by width
                {
                    imgScaleWidth = m_iPanelWidth;
                    imgScaleHeight = (int) Math.round(((double) m_iPanelWidth / (double) imgWidth) *
                            (double) imgHeight);
                } else // Scale by height
                {
                    imgScaleHeight = m_iPanelHeight;
                    imgScaleWidth = (int) Math.round(((double) m_iPanelHeight / (double) imgHeight) *
                            (double) imgWidth);
                }
            } else // Draw it normal size
            {
                imgScaleWidth = imgWidth;
                imgScaleHeight = imgHeight;
            }
            // set the transition time from the array of transition times
            transTime = m_vTransitionLength.elementAt(this.transIdx);
            // prepare the image to go into the function
            Graphics gNext = m_NextImage.getGraphics();
            gNext.drawImage(nextImg, posX, posY, imgScaleWidth, imgScaleHeight, this);
        }
        // Pass the transitions to the draw function.
        m_vTransitions.elementAt(this.transIdx).DrawImageTransition(
                this,
                m_CurrentImage,
                m_NextImage,
                transTime
        );
	}
	
	//--------------------------------------------------------
	/** Override the paint function to draw the image. */
	//--------------------------------------------------------
	public void paint(Graphics g)
	{
		this.paintComponent(g);
		this.paintBorder(g);
		if(this.theImage != null)
		{
			g.drawImage(m_CurrentImage, 0, 0, null);
		}
		else
		{
			g.setColor(Color.BLACK);
			g.drawRect(0, 0, m_iPanelWidth, m_iPanelHeight);
		}
	}
}

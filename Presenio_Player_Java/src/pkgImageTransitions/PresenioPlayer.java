package pkgImageTransitions;
import java.awt.FileDialog;
import javax.imageio.ImageIO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.io.*;
import javax.sound.sampled.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//=============================================================================
/** Class: PresenioPlayer
 *  Purpose: This class implements the main window for an image viewer utility.
 *    It allows the user to select a directory of images (.jpg)
 *    and display them sequentially.  The user can switch images by clicking
 *    a button to move forward or backward in the list of images or by
 *    using a timer to produce a slideshow. The user can also pause and play
 *    music and pause the slideshow using the provided buttons.
 *  Author: Dr. Rick Coleman
 *  Date: April 2008
 *  Modified by Presenio Project Team 10
 *  Date: April 2018
 */
//=============================================================================
public class PresenioPlayer extends JFrame {
    /** Programmer ID */
    public String m_sID = "Dr. Rick Coleman (Modified by Presenio Project Team (Team 10))";

    /* Latch for music to play one at a time */
    CountDownLatch syncLatch = new CountDownLatch(1);

    private Clip clip; // Audio clip
    public List<List> imageList; //Image List
    public String rootFilePath; // Root file path from the save file
    public String filename; // file name from the save file
    public String autoplaySS; // auto play slideshow flag from save file
    public List<String> audioList; // audio paths as an accessible list
    public List<String> imageNamesFromSave; // image names as an accessible list
    public List<String> transitionFromSave; // transitions as an accessible list
    public List<Long> slideLengthFromSave; // slide lengths as an accessible list
    public List<Long> transitionLengthFromSave; // transitions lengths as an accessible list
    public boolean stopFromButton = false; // Flag for people to pause the audio with the pause button
    public int audioCounter; //An index of which audio file is playing
    private AudioInputStream stream; // The audio stream to play the clip from

    /** Main screen width - based on screen width */
    public int m_iScnWidth;

    /** Main screen height - based on screen height */
    public int m_iScnHeight;

    /** Panel displaying the images */
    public ImagePanel m_ImagePanel;

    /** Panel holding the buttons */
    private JPanel m_ButtonPanel;

    /** Select image directory button */
    private JButton m_SelectImageDirBtn;

    /** Switch to previous image button */
    private JButton m_PrevImageBtn;

    /** Switch to next image button */
    private JButton m_NextImageBtn;
    private JButton pauseImageBtn; // pause image button
    private JButton playImageBtn; // play image button
    //------------------------------------------
    // Display option variables
    //------------------------------------------
    /** Scale images flag */
    private boolean m_bScaleImages = true;
    /** Change images manually flag */
    private boolean m_bChangeManually = true;

    /** Time delay is using timer to change */
    private int m_iTimeDelay = 5; // default setting

    //------------------------------------------
    // Miscellaneous variables
    //------------------------------------------
    /** Image directory to show */
    private String m_slideShowFile;

    /** Vector of image names */
    private Vector<String> m_vImageNames = null;

    /** Vector of image transitions */
    private Vector<String> imageTransitions = null;

    /** Index of the current image */
    private int m_iCurImageIdx;

    /** Image currently displayed */
    private BufferedImage m_TheImage = null;

    /** Timer for slideshows */
    private Timer m_SSTimer;

    private String pathDelimiter; // the file delimiter to assemble paths if necessary

    //---------------------------------------------------
    /** Default constructor */
    //---------------------------------------------------
    public PresenioPlayer()
    {
        audioCounter = 0; // Set the initial audio counter to zero
        // ==== Set file delimiter for different OS ==== //
        String OS = System.getProperty("os.name"); // get the name of the OS
        if (OS.contains("indow")){ // if it's windows
            pathDelimiter = "\\"; // escape the backslash
        } else {
            pathDelimiter = "/"; // otherwise forwardslash
        }

        //------------------------------------------
        // Set all parameters for this JFrame object
        //------------------------------------------

        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        m_iScnWidth = d.width - 20;
        m_iScnHeight = d.height - 80;
        // Allow users to close with the X button
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocation(10, 10); // offsest from the top of the screen
        this.setTitle("Presenio Player");
        this.setSize(m_iScnWidth, m_iScnHeight);
        this.setResizable(false);
        this.getContentPane().setLayout(null); // We'll do our own layouts, thank you.
        this.getContentPane().setBackground(Color.gray); // Set visible area to gray
        // Create the image panel
        m_ImagePanel = new ImagePanel(this);
        m_ImagePanel.setSize(m_iScnWidth, m_iScnHeight-100); // window appears smaller than screen
        this.getContentPane().add(m_ImagePanel); // Add the panel to the window

        // Create the button panel
        m_ButtonPanel = new JPanel();
        m_ButtonPanel.setSize(this.getSize().width, 100);
        m_ButtonPanel.setLocation(0, this.getSize().height-100);
        m_ButtonPanel.setOpaque(false); // Set the panel color
        // Use the default Flow Layout manager
        this.getContentPane().add(m_ButtonPanel);

        // Create the select image directory button
        m_SelectImageDirBtn = new JButton(new ImageIcon(getClass().getResource("ic_create_new_folder_white_24dp.png")));
        m_SelectImageDirBtn.setSize(48, 48);
        m_SelectImageDirBtn.setContentAreaFilled(false); // Do not show a background on the button
        m_SelectImageDirBtn.setBorderPainted(false); // Do not show a border on the button
        m_SelectImageDirBtn.setOpaque(false);
        m_SelectImageDirBtn.setToolTipText("Click to select a presentation to view.");
        m_SelectImageDirBtn.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        doSlideShow();
                    }
                });
        m_ButtonPanel.add(m_SelectImageDirBtn);

        // Create the previous image button
//		m_PrevImageBtn = new JButton(new ImageIcon("Images/BackArrow.jpg"));
        m_PrevImageBtn = new JButton(new ImageIcon(getClass().getResource("skip-previous-circle.png")));
        m_PrevImageBtn.setSize(48, 48);
        m_PrevImageBtn.setContentAreaFilled(false);
        m_PrevImageBtn.setBorderPainted(false);
        m_PrevImageBtn.setOpaque(false);
        m_PrevImageBtn.setToolTipText("View previous image.");
        m_PrevImageBtn.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        //	Show the previous image
                        showPreviousImage();
                    }
                });
        m_ButtonPanel.add(m_PrevImageBtn);

        // Create the pause button
        pauseImageBtn = new JButton(new ImageIcon(getClass().getResource("pause-circle.png")));
        pauseImageBtn.setSize(48, 48);
        pauseImageBtn.setContentAreaFilled(false);
        pauseImageBtn.setBorderPainted(false);
        pauseImageBtn.setOpaque(false);
        pauseImageBtn.setToolTipText("Pause");
        pauseImageBtn.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        // Pause the slideshow
                        stopFromButton = true; // Set the pause flag
                        if(!m_bChangeManually) // if the slideshow is autoplay
                            m_SSTimer.stop();   // pause the slideshow
                        clip.stop();           // stop the audio
                    }
                });
        m_ButtonPanel.add(pauseImageBtn);

        // Create the play button
        playImageBtn = new JButton(new ImageIcon(getClass().getResource("play-circle.png")));
        playImageBtn.setSize(48, 48);
        playImageBtn.setContentAreaFilled(false);
        playImageBtn.setBorderPainted(false);
        playImageBtn.setOpaque(false);
        playImageBtn.setToolTipText("Play");
        playImageBtn.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        clip.start();       // start the audio
                        if(!m_bChangeManually)  // if the slideshow is autoplay
                            m_SSTimer.start();  // start the slideshow again
                    }
                });
        m_ButtonPanel.add(playImageBtn);

        // Create the next image button
//		m_NextImageBtn = new JButton(new ImageIcon("Images/NextArrow.jpg"));
        m_NextImageBtn = new JButton(new ImageIcon(getClass().getResource("skip-next-circle.png")));
        m_NextImageBtn.setSize(48, 48);
        m_NextImageBtn.setContentAreaFilled(false);
        m_NextImageBtn.setBorderPainted(false);
        m_NextImageBtn.setOpaque(false);
        m_NextImageBtn.setToolTipText("View next image.");
        m_NextImageBtn.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        //	Show the next image
                        if(m_iCurImageIdx <= m_vImageNames.size()-1)
                            showNextImage();
                    }
                });
        m_ButtonPanel.add(m_NextImageBtn);

        // Make the window visible
        this.setVisible(true);
    }

    //---------------------------------------------------
    // Set the current image index into the image panel as well
    // so that slideshow transitions will sync
    //---------------------------------------------------
    public void setImagePanelIdx() {
        m_ImagePanel.setCurIdx(m_iCurImageIdx);
    }


    //---------------------------------------------------
    /** Start the slideshow from the directory */
    //---------------------------------------------------
    private void doSlideShow() {
        m_iCurImageIdx = 0; // set the current image index to 0
        setImagePanelIdx(); // sync with the image panel index
        try {
            getImageDir(); // get the file from the directory to parse
        } catch(ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(m_slideShowFile != null) // if the slideshow file is good
        {
            setImagePanelIdx();         //sync the image panel index
            showImage(m_iCurImageIdx);  // Show first image
            playNextAudio();            // start the audio
            if(!m_bChangeManually)      // if it's autoplay
                doTimerSlideShow();     // start the timer
        }
    }

    //---------------------------------------------------
    /** Play through the list of audio files taken from the save file */
    //---------------------------------------------------
    private void playNextAudio(){
        try {
            if(audioCounter <= audioList.size()) {
                if (audioCounter == 0) { // on the first pass
                    // Play a silent file to advance the slideshow
                    // The lock freezes the GUI until
                    // after the first time it's released.
                    stream = AudioSystem.getAudioInputStream(getClass().getResource("Silence.wav"));
                } else { // If it's not the first file
                    File inStream = new File(audioList.get(audioCounter - 1)); // Get an audio file
                    stream = AudioSystem.getAudioInputStream(inStream);     // assign it to a stream
                }
                clip = null; // clear the previous clip
                clip = AudioSystem.getClip();   // init clip as if it's new
                // Listener which allow method return once sound is completed
                clip.addLineListener(e -> { // set a listener for the pause button
                    if (e.getType() == LineEvent.Type.STOP) { // if paused
                        if(stopFromButton) {    // if the user pressed pause
                            stopFromButton = false; // unset the flag and stop the music
                        } else { // if the stream has closed because the file has played
                            clip.close();   // close the clip
                            syncLatch.countDown();  // release the latch
                            audioCounter++;         // increment the counter
                            playNextAudio();        // restart the function
                        }
                    }
                });
                clip.open(stream);  // open the audio clip
                clip.start();       // start the audio clip
                try {
                    syncLatch.await(); // set the latch to only play one at a time
                } catch (java.lang.InterruptedException f) {
                    f.printStackTrace();
                }
            }
        } catch (javax.sound.sampled.LineUnavailableException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        } catch (javax.sound.sampled.UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
    }

    //----------------------------------------------------------------------
    /** Show an open file dialog box in order to get the directory of
     *   images to display. */
    //----------------------------------------------------------------------
    private void getImageDir() throws ParseException, FileNotFoundException, IOException {
        // load file
        FileDialog fd = new FileDialog(this, "Choose a file", FileDialog.LOAD);
        fd.setFile("*.json");   // of type JSON only
        fd.setVisible(true);    // show only JSON files
        String filename = fd.getDirectory() + fd.getFile(); // set the path
        if (filename == null)   // if there is no filename
            System.out.println("You cancelled the choice"); // they cancelled
        else
            m_slideShowFile = filename; // there is a slideshow file
            getFileContents(filename);  // get the file contents
    }

    //---------------------------------------------------
    /** Save File Parser
     * Sets information from the save file into
     * arrays in PresenioPlayer */
    //---------------------------------------------------
    private void getFileContents(String fileName) throws ParseException, FileNotFoundException, IOException {
        JSONParser parser = new JSONParser();   // create the parser
        Reader reader = new FileReader(fileName);   // create the reader
        JSONObject jsonObject = (JSONObject) parser.parse(reader);  // create the JSON object
        // Since it doesn't know what type of things we have saved
        // in the JSON file, have to manually cast everything.
        // Take in the following as strings
        rootFilePath = (String) jsonObject.get("rootFilePath");
        filename = (String) jsonObject.get("filename");
        autoplaySS = (String) jsonObject.get("autoplay");
        //Tell it it's a list of lists
        // must init before calling .add
        imageList = new ArrayList<List>();
        audioList = new ArrayList<String>();
        //break free of the JSONObject
        List bridge = (List) jsonObject.get("images");
        if(bridge != null) {     // if there is a list of lists to handle
            //really break free of the JSONObject
            // addAll works the same as for(int i = 0; i < bridge.size(); i++){ add }
            imageList.addAll((List) bridge); // set into imageList
        }
        // Build arrays of the information like transition and slide times
        this.buildArrays();
        // Pass the transition list to the Image Panel object
        this.m_ImagePanel.setImageTransitions(imageTransitions);
        List audioBridge = (List) jsonObject.get("audio"); // Get the list for audio
        if(audioBridge != null) { // if there is a list of audio to handle
            for (int i = 0; i < audioBridge.size(); i++) {
                audioList.add((String) audioBridge.get(i)); // add into audio list
            }
        }
        reader.close(); // close the reader
        this.checkIfManual(); // check if the manual flag has been set
    }

    //---------------------------------------------------
    /** Check if the manual flag has been set
     * If so, diable prev/next buttons.
     * If not, set the autoplay flag */
    //---------------------------------------------------
    private void checkIfManual() {
        if (autoplaySS.contains("true")) { // slideshow is on a timer
            // Disable the previous and next buttons while the slideshow runs
            m_PrevImageBtn.setEnabled(false);
            m_NextImageBtn.setEnabled(false);
            m_bChangeManually = false; // set the change manually flag
        } else if (autoplaySS.contains("false")) {
            // slideshow is autoplaySS advanced
            m_bChangeManually = true;
        }
    }

    //----------------------------------------------------------------------
    /** Build the list of images to show */
    //----------------------------------------------------------------------
    private void buildArrays()
    {
        //must init before .add function
        imageNamesFromSave = new ArrayList<String>();
        transitionFromSave = new ArrayList<String>();
        //program does not accept that these are integers
        slideLengthFromSave = new ArrayList<Long>();
        transitionLengthFromSave = new ArrayList<Long>();
        for(int i = 0; i < imageList.size(); i++) {
            //parse out into separate arrays
            imageNamesFromSave.add(rootFilePath + pathDelimiter + (String) (imageList.get(i)).get(0));
            transitionFromSave.add((String) (imageList.get(i)).get(1));
            slideLengthFromSave.add((Long) (imageList.get(i)).get(2));
            transitionLengthFromSave.add((Long) (imageList.get(i)).get(3));
        }
        //instantiate m_vImageNames
        m_vImageNames = new Vector<String>();
        imageTransitions = new Vector<String>();
        Vector transLengths = new Vector<Long>();
        Vector slideLengths = new Vector<Long>();
        //add into image names vector
        m_vImageNames.addAll(imageNamesFromSave);
        imageTransitions.addAll(transitionFromSave);
        transLengths.addAll(transitionLengthFromSave);
        slideLengths.addAll(slideLengthFromSave);
        // set the transition lengths in the image panel
        m_ImagePanel.setTransLengthVector(transLengths);
    }

    //----------------------------------------------------------------------
    /** Show the image at index. */
    //----------------------------------------------------------------------
    private void showImage(int idx)
    {
        File		imageFile; // the jpg or gif file
        // Make sure we have an image file
        if(idx > m_vImageNames.size())
        {
            JOptionPane.showMessageDialog(this,
                    "Error: Unable to display image " + idx + ". Does not exist.",
                    "Error Loading Image", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!(m_iCurImageIdx == m_vImageNames.size())) {
            imageFile = new File((String) (m_vImageNames.elementAt(idx)));
            if (!imageFile.exists()) // If we failed to opened it
            {
                JOptionPane.showMessageDialog(this,
                        "Error: Unable to load " + (String) (m_vImageNames.elementAt(idx)),
                        "Error Loading Image", JOptionPane.ERROR_MESSAGE);
                return;
            }
            // Load the image
            // Use ImageIO and pass a BufferedImage.TYPE_INT_RGB to ImagePanel
            if (m_TheImage != null)
                m_TheImage = null; // Clear the previous image
            try {
                // read the file
                m_TheImage = ImageIO.read(imageFile);
                if (m_TheImage.getType() != BufferedImage.TYPE_INT_RGB) {
                    BufferedImage bi2 =
                            new BufferedImage(m_TheImage.getWidth(), m_TheImage.getHeight(), BufferedImage.TYPE_INT_RGB);
                    Graphics big = bi2.getGraphics();
                    big.drawImage(m_TheImage, 0, 0, null);
                    m_TheImage = bi2;
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this,
                        "Error: Unable to load " + (String) (m_vImageNames.elementAt(idx)),
                        "Error Loading Image", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        else if (m_iCurImageIdx == (m_vImageNames.size()))
            m_TheImage = null;
        m_ImagePanel.setImage(m_TheImage);
    }

    //----------------------------------------------------------------------
    /** Show the previous image. */
    //----------------------------------------------------------------------
    private void showPreviousImage()
    {
        // if it's not trying to advance to a nonexistent index
        if(m_iCurImageIdx > 0)
        {
            m_iCurImageIdx--; // Decrement to previous image
            // if it's going backwards from the slide past the last image, special case
            if (m_iCurImageIdx >= m_vImageNames.size())
                setImagePanelIdx(); // set the image panel index before the image is shown
            showImage(m_iCurImageIdx); // Show it
            if(m_iCurImageIdx < m_vImageNames.size())
                setImagePanelIdx(); // otherwise  set the panel index after the image is shown
        }
    }

    //----------------------------------------------------------------------
    /** Show the next image. */
    //----------------------------------------------------------------------
    private void showNextImage()
    {
        m_iCurImageIdx++; // Increment to next image
        setImagePanelIdx(); // set the panel index
        showImage(m_iCurImageIdx); // Show the image
    }

    //----------------------------------------------------------------------
    /** Show the next image. */
    //----------------------------------------------------------------------
    private void doTimerSlideShow()
    {
        // Create a javax.swing.timer
        // Set the delay to be the current time delay
        m_SSTimer = new Timer(m_iTimeDelay * 1000,
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        // Show the next image
                        if ((m_iCurImageIdx < m_vImageNames.size())) {
                            // if advancing to the last imgage, manually set
                            if(m_iCurImageIdx == m_vImageNames.size()+1)
                                m_iTimeDelay = 5;
                            else //set the image based on the slide length from save
                                m_iTimeDelay = slideLengthFromSave.indexOf(m_iCurImageIdx+1);
                            showNextImage(); // show the next image
                        } else {
                            m_SSTimer.stop(); // if you're on the last slide
                            // Enable the previous and next buttons again
                            m_PrevImageBtn.setEnabled(true);
                            m_NextImageBtn.setEnabled(true);
                        }
                    }
                });
        m_SSTimer.setRepeats(true); // Repeat till we kill it
        m_SSTimer.start();  // Start the timer
    }

    //----------------------------------------------------------------------
    /** Main function for this demonstration
     * @param args - Array of strings from the command line
     * Provided by instructor.
     */
    //----------------------------------------------------------------------
    public static void main(String[] args)
    {
        // When you start this application this function gets called by the
        //  operating system.  Main just creates an ImageViewer object.
        //  To follow the execution trail from here go to the ImageViewer
        //  constructor.
        PresenioPlayer IV = new PresenioPlayer();
    }

}
